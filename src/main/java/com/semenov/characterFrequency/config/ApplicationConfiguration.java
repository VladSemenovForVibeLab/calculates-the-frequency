package com.semenov.characterFrequency.config;

import io.minio.MinioClient;
import io.minio.MinioProperties;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Конфигурация приложения.
 */
@Configuration
@RequiredArgsConstructor
public class ApplicationConfiguration {
    /**
     * Метод создает бин OpenAPI для описания документации REST API.
     *
     * @return экземпляр класса OpenAPI
     */
    @Bean
    public OpenAPI OpenAPI() {
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList("bearerAuthentication"))
                .components(
                        new Components()
                                .addSecuritySchemes("bearerAuthentication",
                                        new SecurityScheme()
                                                .type(SecurityScheme.Type.HTTP)
                                                .scheme("bearer")
                                                .bearerFormat("JWT")
                                )
                )
                .info(new Info()
                        .title("REST API, вычисляющее частоту встречи символов по заданной строке")
                        .description("Результат должен быть отсортирован по убыванию количества вхождений символа в заданную строку.")
                        .version("0.0.1"));
    }
}
